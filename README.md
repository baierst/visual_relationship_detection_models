# Introduction

This repository contains the code for the experiments published in the papers [Improving Visual Relationship Detection using Semantic Modeling of Scene Descriptions](https://link.springer.com/chapter/10.1007/978-3-319-68288-4_4) and [Improving Information Extraction from Images with Learned Semantic Models](https://www.ijcai.org/proceedings/2018/0724.pdf).

The training data and evaluation code can be found here: https://github.com/Prof-Lu-Cewu/Visual-Relationship-Detection

# Installation

Please make sure that you use keras with the theano backend. The keras.json setting should look as follows:

```
{
"image_data_format": "channels_first",
"image_dim_ordering": "th",
"epsilon": 1e-07,
"floatx": "float32",
"backend": "theano"
}
````

# Getting Started

The file `experiment.py` contains the models and experiments of the Bayesian Fusion model published in [Improving Visual Relationship Detection using Semantic Modeling of Scene Descriptions](https://link.springer.com/chapter/10.1007/978-3-319-68288-4_4).
The file `experiments_conditional.py` contains the models and experiments of the conditional multi-way model published in Improving Information Extraction from Images with Learned Semantic Models](https://www.ijcai.org/proceedings/2018/0724.pdf)

The code in `experiment.py` and `experiments_conditional.py` generates the model outputs. To get the evaluation metrics, as reported in the papers run `evaluation/evaluation.m`. The evaluation code originates from https://github.com/Prof-Lu-Cewu/Visual-Relationship-Detection.

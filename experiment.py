from scipy.io.matlab import loadmat, savemat
import pickle as pkl
import numpy as np
import cv2
from utils.image_utils import extract_region, get_bbox_p
import img_models

filenames = pkl.load(open('data/filenames.pkl', 'rb'), encoding='latin1')
annotations_test = pkl.load(open('data/annotations_test.pkl', 'rb'), encoding='latin1')
annotations_rcnn = pkl.load(open('data/annotations_rcnn.pkl', 'rb'), encoding='latin1')
index_to_object = pkl.load(open('data/index_to_object.pkl', 'rb'), encoding='latin1')

P = loadmat('data/complex.mat')['P']

entity_preds = pkl.load(open('data/entity_preds.pkl', 'rb'), encoding='latin1')
predicate_preds = pkl.load(open('data/predicate_preds.pkl', 'rb'), encoding='latin1')

entity_model = img_models.build_entity_model(weights='data/entity_model.w')

rlp_confs_ours = [[]] * 1000
rlp_labels_ours = [[]] * 1000
sub_bboxes_ours = [[]] * 1000
obj_bboxes_ours = [[]] * 1000

original = True

for i in range(0, 1000):

    if i % 100 == 0:
        print(i)

    filename = filenames[i][0]

    if annotations_test[filename]['tuples'] == [] or annotations_rcnn[filename] == []:
        continue

    candidate_boxes = annotations_rcnn[filename]

    rlp_labels_ours[i] = []
    rlp_confs_ours[i] = []
    sub_bboxes_ours[i] = []
    obj_bboxes_ours[i] = []

    if not original:
        bgr = cv2.imread('data/test/' + filename)

        def get_region(box, bgr):
            region = extract_region(bgr, box, rcnn=True)
            region = cv2.resize(region.astype('float32'), (224, 224)).transpose(2, 0, 1)
            return region

        regions = np.array([get_region(box, bgr)for box in candidate_boxes])

        prob = entity_model.predict(regions)

    pair_count = 0
    for i_s in range(len(candidate_boxes)):

        if original:
            s_given_x = entity_preds[i][i_s]
        else:
            s_given_x = prob[i_s]

        for i_o in range(len(candidate_boxes)):
            if i_s == i_o:
                continue

            if original:
                o_given_x = entity_preds[i][i_o]
                p_given_x = predicate_preds[i][pair_count]
                pair_count += 1
            else:
                o_given_x = prob[i_o]
                p_given_x = predicate_preds[i][pair_count]

            scores = np.einsum('i,j,k->ijk',  p_given_x, s_given_x, o_given_x) * P
            p, s, o = np.unravel_index(np.argmax(scores), P.shape)
            rlpScore = scores[p, s, o]

            rlp_labels_ours[i].append((float(s + 1), float(p + 1), float(o + 1)))
            rlp_confs_ours[i].append(rlpScore)
            sub_bboxes_ours[i].append(candidate_boxes[i_s].astype('float'))
            obj_bboxes_ours[i].append(candidate_boxes[i_o].astype('float'))

savemat('evaluation/result_complex.mat', {'rlp_labels_ours': rlp_labels_ours,
                                            'rlp_confs_ours': rlp_confs_ours,
                                            'sub_bboxes_ours': sub_bboxes_ours,
                                            'obj_bboxes_ours': obj_bboxes_ours})


from os import listdir
from os.path import isfile, join
import numpy as np

import cv2

class ImageLoader(object):

    def __init__(self, image_path, buffer_size=100):

        self.image_path = image_path
        self.buffer_size = buffer_size

        self.reset_iterator()

    def __iter__(self):
        return self

    def reset_iterator(self):

        import re
        numbers = re.compile(r'(\d+)')
        def numericalSort(value):
            parts = list(map(int, numbers.split(value)[1::2]))
            id = parts[0] * 10 + parts[1]
            return id


        self.buffer_size = self.buffer_size
        self.onlyfiles = sorted([f for f in listdir(self.image_path) if isfile(join(self.image_path, f))], key=numericalSort)
        self.onlyfiles_index = 0
        self.image_path = self.image_path
        self.remaining = sum([1 for f in listdir(self.image_path) if isfile(join(self.image_path, f))])


    def __next__(self):

        if self.remaining < 1:
            self.reset_iterator()
            raise StopIteration

        images = []

        if self.remaining > self.buffer_size:
            iter_size = self.buffer_size
        else:
            iter_size = self.remaining

        for n in range(0, iter_size):
            filename = self.onlyfiles[self.onlyfiles_index]
            self.onlyfiles_index += 1
            im = cv2.imread(join(self.image_path, filename))
            # im = im.transpose((2, 0, 1))

            image = {}
            image['bgr'] = im
            image['filename'] = filename

            if 'gif' in filename:
                print(filename)

            images.append(image)

        self.remaining -= self.buffer_size

        return images

def extract_representation(image_batch, rep_model):

    image_batch = np.array([cv2.resize(image.astype('float32'), (224, 224)) for image in image_batch])

    image_batch = image_batch.transpose(0, 3, 1, 2)

    image_batch -= np.array([103.939, 116.779, 123.68], dtype=np.float32).reshape((1, 3, 1, 1))

    return rep_model.predict(image_batch)

def extract_region(bgr, bbox, rcnn=False):

    if rcnn:
        x1 = bbox[0]
        y1 = bbox[1]
        x2 = bbox[2]
        y2 = bbox[3]
    else:
        y1 = bbox[0]
        y2 = bbox[1]
        x1 = bbox[2]
        x2 = bbox[3]

    region_length_x = x2 - x1
    region_length_y = y2 - y1

    x_scale = 224. / region_length_x
    y_scale = 224. / region_length_y

    p = 16

    padding_x = p / x_scale / 2
    padding_y = p / y_scale / 2

    region_y1 = y1 - padding_y
    region_y2 = y2 + padding_y
    region_x1 = x1 - padding_x
    region_x2 = x2 + padding_x

    if region_y1 < 0:
        region_y1 = 0
    if region_y2 > bgr.shape[0]:
        diff = region_y2 - bgr.shape[0]
        bgr = cv2.copyMakeBorder(bgr, 0, int(diff) + 5, 0, 0, cv2.BORDER_CONSTANT, value=bgr.mean(axis=(0, 1)))
    if region_x1 < 0:
        region_x1 = 0
    if region_x2 > bgr.shape[1]:
        diff = region_x2 - bgr.shape[1]
        bgr = cv2.copyMakeBorder(bgr, 0, 0, 0, int(diff) + 5, cv2.BORDER_CONSTANT, value=bgr.mean(axis=(0, 1)))

    sub_img = bgr[int(region_y1):int(region_y2), int(region_x1):int(region_x2)].copy()

    return sub_img


def get_bbox_p(bbox_s, bbox_o):
    y1_s = bbox_s[0]
    y2_s = bbox_s[1]
    x1_s = bbox_s[2]
    x2_s = bbox_s[3]

    y1_o = bbox_o[0]
    y2_o = bbox_o[1]
    x1_o = bbox_o[2]
    x2_o = bbox_o[3]

    y1_p = min(y1_s, y1_o, y2_s, y2_o)
    y2_p = max(y1_s, y1_o, y2_s, y2_o)
    x1_p = min(x1_s, x1_o, x2_s, x2_o)
    x2_p = max(x1_s, x1_o, x2_s, x2_o)

    bbox_p = [y1_p, y2_p, x1_p, x2_p]

    return bbox_p
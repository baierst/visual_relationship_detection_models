from keras.layers import Input, Multiply, Embedding, Flatten, Lambda, Concatenate, Dense, Merge
from keras.optimizers import Adam
from keras import regularizers
from keras.models import Model
from keras.callbacks import EarlyStopping
import keras.backend as K
import numpy as np
import pickle as pkl
from utils.image_utils import ImageLoader
import scipy.io as sio
from utils.embedding3d import Embedding3D

NB_ENTITIES = 100
NB_PREDICATES = 70


def build_complex_model(rank):
    s_input = Input(shape=(1,), dtype='int32', name='subject_input')
    p_input = Input(shape=(1,), dtype='int32', name='predicate_input')
    o_input = Input(shape=(1,), dtype='int32', name='object_input')

    entityEmbedding_real = Embedding(output_dim=rank, input_dim=NB_ENTITIES, input_length=1,
                                     name='embedding_entity_real')
    entityEmbedding_im = Embedding(output_dim=rank, input_dim=NB_ENTITIES, input_length=1, name='embedding_entity_im', )

    predicateEmbedding_real = Embedding(output_dim=rank, input_dim=NB_PREDICATES, input_length=1,
                                        name='embedding_predicate_real')
    predicateEmbedding_im = Embedding(output_dim=rank, input_dim=NB_PREDICATES, input_length=1,
                                      name='embedding_predicate_im')

    s_embedding_real = entityEmbedding_real(s_input)
    s_embedding_real = Flatten()(s_embedding_real)

    s_embedding_im = entityEmbedding_im(s_input)
    s_embedding_im = Flatten()(s_embedding_im)

    p_embedding_real = predicateEmbedding_real(p_input)
    p_embedding_real = Flatten()(p_embedding_real)

    p_embedding_im = predicateEmbedding_im(p_input)
    p_embedding_im = Flatten()(p_embedding_im)

    o_embedding_real = entityEmbedding_real(o_input)
    o_embedding_real = Flatten()(o_embedding_real)

    o_embedding_im = entityEmbedding_im(o_input)
    o_embedding_im = Flatten()(o_embedding_im)

    rep1 = Multiply()([s_embedding_real, p_embedding_real, o_embedding_real])
    rep2 = Multiply()([s_embedding_im, p_embedding_real, o_embedding_im])
    rep3 = Multiply()([s_embedding_real, p_embedding_im, o_embedding_im])
    rep4 = Multiply()([s_embedding_im, p_embedding_im, o_embedding_real])

    rep = Lambda(lambda x: x[0] + x[1] + x[2] - x[3], output_shape=(None, rank))([rep1, rep2, rep3, rep4])

    out = Lambda(lambda x: K.exp(K.sum(x, axis=1)[:, None]), output_shape=(1,))(rep)

    model = Model(inputs=[s_input, p_input, o_input], outputs=out)

    return model


def build_multiway_model(rank):
    s_input = Input(shape=(1,), dtype='int32', name='subject_input')
    p_input = Input(shape=(1,), dtype='int32', name='predicate_input')
    o_input = Input(shape=(1,), dtype='int32', name='object_input')

    # init_weights_e = np.random.uniform(0.32, 0.33, size=(NB_ENTITIES, rank))
    # init_weights_p = np.random.uniform(0.32, 0.33, size=(NB_PREDICATES, rank))

    entityEmbedding = Embedding(output_dim=rank, input_dim=NB_ENTITIES, input_length=1, name='embedding_entity')
    predicateEmbedding = Embedding(output_dim=rank, input_dim=NB_PREDICATES, input_length=1, name='embedding_predicate')

    s_embedding = entityEmbedding(s_input)
    s_embedding = Flatten()(s_embedding)

    p_embedding = predicateEmbedding(p_input)
    p_embedding = Flatten()(p_embedding)

    o_embedding = entityEmbedding(o_input)
    o_embedding = Flatten()(o_embedding)

    rep = Concatenate()([s_embedding, p_embedding, o_embedding])

    h = Dense(30, activation='relu')(rep)

    out = Dense(1)(h)

    out = Lambda(lambda x: K.exp(x))(out)

    model = Model(inputs=[s_input, p_input, o_input], outputs=out)

    return model


def build_rescal_model(rank):
    s_input = Input(shape=(1,), dtype='int32', name='subject_input')
    p_input = Input(shape=(1,), dtype='int32', name='predicate_input')
    o_input = Input(shape=(1,), dtype='int32', name='object_input')

    entityEmbedding = Embedding(output_dim=rank, input_dim=NB_ENTITIES, input_length=1, name='embedding_entity')

    s_embedding = entityEmbedding(s_input)
    s_embedding = Flatten()(s_embedding)

    R_init = np.repeat(np.diag(np.ones(rank))[np.newaxis, :, :], NB_PREDICATES, axis=0)
    p_embedding = Embedding3D(input_dim=NB_PREDICATES, output_dim1=rank, output_dim2=rank,
                              name='embedding_predicate', weights=[R_init])(p_input)

    o_embedding = entityEmbedding(o_input)
    o_embedding = Flatten()(o_embedding)

    def rescal(x):
        sp = K.batch_dot(x[0], x[1], axes=1)
        spo = K.batch_dot(sp, x[2], axes=1)
        return K.exp(spo)

    out = Merge(mode=lambda x: rescal(x), output_shape=(1,))([s_embedding, p_embedding, o_embedding])

    model = Model(inputs=[s_input, p_input, o_input], outputs=out)

    return model


def build_distmult_model(rank):
    s_input = Input(shape=(1,), dtype='int32', name='subject_input')
    p_input = Input(shape=(1,), dtype='int32', name='predicate_input')
    o_input = Input(shape=(1,), dtype='int32', name='object_input')

    entityEmbedding = Embedding(output_dim=rank, input_dim=NB_ENTITIES, input_length=1, name='embedding_entity', )
    predicateEmbedding = Embedding(output_dim=rank, input_dim=NB_PREDICATES, input_length=1, name='embedding_predicate')

    s_embedding = entityEmbedding(s_input)
    s_embedding = Flatten()(s_embedding)

    p_embedding = predicateEmbedding(p_input)
    p_embedding = Flatten()(p_embedding)

    o_embedding = entityEmbedding(o_input)
    o_embedding = Flatten()(o_embedding)

    rep = Multiply()([s_embedding, p_embedding, o_embedding])

    out = Lambda(lambda x: K.exp(K.sum(x, axis=1)[:, None]), output_shape=(1,))(rep)

    model = Model(inputs=[s_input, p_input, o_input], outputs=out)

    return model


def load_train_data(full=False):
    all_triples = {}

    for s in range(NB_ENTITIES):
        for p in range(NB_PREDICATES):
            for o in range(NB_ENTITIES):
                all_triples[(s, p, o)] = 0

    annotations_train = pkl.load(open('data/annotations_train.pkl', 'rb'), encoding='latin1')
    train_loader = ImageLoader('data/train', buffer_size=1)

    triples = []

    for image in train_loader:

        if annotations_train[image[0]['filename']]['tuples'] == []:
            continue

        tuples = annotations_train[image[0]['filename']]['tuples']
        s, bs, p, bo, o = zip(*tuples)
        triples += zip(s, p, o)

    for s, p, o in triples:
        all_triples[(s, p, o)] += 1

    x_train = zip(*all_triples.keys())

    x_train = [np.array(e) for e in x_train]


    y_train = np.array(list(all_triples.values()))

    if full:
        return x_train, y_train
    else:
        nonzero_positions = np.where(y_train != 0)[0]
        val_positions = np.random.choice(nonzero_positions, size=int(nonzero_positions.shape[0]*0.05), replace=False)

        x_val = [0, 0, 0]

        x_val[0] = x_train[0][val_positions]
        x_val[1] = x_train[1][val_positions]
        x_val[2] = x_train[2][val_positions]
        y_val = y_train[val_positions]

        x_train[0] = np.delete(x_train[0], val_positions)
        x_train[1] = np.delete(x_train[1], val_positions)
        x_train[2] = np.delete(x_train[2], val_positions)
        y_train = np.delete(y_train, val_positions)

        return x_train, y_train, x_val, y_val


def train_model():

    rank = 10

    x_train, y_train = load_train_data(full=True)

    Ps = []
    for i in range(20):

        model = build_complex_model(rank)

        opt = Adam(0.1)
        model.compile(opt, 'poisson')

        model.fit(x_train, y_train, batch_size=700000, epochs=200, verbose=2)

        P = np.zeros((NB_ENTITIES, NB_PREDICATES, NB_ENTITIES))
        P[x_train] = model.predict(x_train)[:, 0]
        P = np.swapaxes(P, 0, 1)

        Ps.append(P)

    P = np.mean(Ps, axis=0)

    # MARGENALZING
    P_p = np.mean(P, axis=(1, 2)) / np.mean(P, axis=(1, 2)).max()
    P_s = np.mean(P, axis=(0, 2)) / np.mean(P, axis=(0, 2)).max()
    P_o = np.mean(P, axis=(0, 1)) / np.mean(P, axis=(0, 1)).max()

    # Laplacian SMOOTHING
    P_p = (P_p + 2) / 3
    P_s = (P_s + 1) / 2
    P_o = (P_o + 1) / 2

    denom = np.einsum('i,j,k->ijk', P_p, P_s, P_o)
    P_norm = P / denom

    sio.savemat('data/complex.mat', {'P': P_norm})


if __name__ == "__main__":
    train_model()

from scipy.io.matlab import loadmat, savemat
import pickle as pkl
import numpy as np
from conditional_models import build_multiway_model_with_image
from conditional_models import build_multiway_model_no_image

filenames = pkl.load(open('data/filenames.pkl', 'rb'), encoding='latin1')
annotations_test = pkl.load(open('data/annotations_test.pkl', 'rb'), encoding='latin1')
annotations_rcnn = pkl.load(open('data/annotations_rcnn.pkl', 'rb'), encoding='latin1')
index_to_object = pkl.load(open('data/index_to_object.pkl', 'rb'), encoding='latin1')

entity_preds = pkl.load(open('data/entity_preds.pkl', 'rb'), encoding='latin1')
predicate_preds = pkl.load(open('data/predicate_preds.pkl', 'rb'), encoding='latin1')

test_ip_reps = pkl.load(open('data/test_ip_reps.pkl', 'rb'), encoding='latin1')

model = build_multiway_model_with_image(20)
model.load_weights('data/conditional_with_image.w')

rlp_confs_ours = [[]] * 1000
rlp_labels_ours = [[]] * 1000
sub_bboxes_ours = [[]] * 1000
obj_bboxes_ours = [[]] * 1000

for i in range(0, 1000):

    if i % 100 == 0:
        print(i)

    filename = filenames[i][0]

    if annotations_test[filename]['tuples'] == [] or len(annotations_rcnn[filename]) < 2:
        continue

    candidate_boxes = annotations_rcnn[filename]

    rlp_labels_ours[i] = []
    rlp_confs_ours[i] = []
    sub_bboxes_ours[i] = []
    obj_bboxes_ours[i] = []

    pair_count = 0
    for i_s in range(len(candidate_boxes)):

        s_given_x = entity_preds[i][i_s]

        for i_o in range(len(candidate_boxes)):
            if i_s == i_o:
                continue

            o_given_x = entity_preds[i][i_o]

            s = np.argmax(s_given_x)
            o = np.argmax(o_given_x)

            rep_p = test_ip_reps[i][pair_count]
            p_given_so = model.predict([np.expand_dims(np.array(s), 1), np.expand_dims(np.array(o), 1), np.expand_dims(rep_p, 0)]).flatten()
            rlpScore = np.max(s_given_x) * np.max(o_given_x) * np.max(p_given_so)


            p = np.argmax(p_given_so)

            rlp_labels_ours[i].append((float(s + 1), float(p + 1), float(o + 1)))
            rlp_confs_ours[i].append(rlpScore)
            sub_bboxes_ours[i].append(candidate_boxes[i_s].astype('float'))
            obj_bboxes_ours[i].append(candidate_boxes[i_o].astype('float'))

            pair_count += 1

    savemat('evaluation/result_conditional.mat', {'rlp_labels_ours': rlp_labels_ours,
                                                'rlp_confs_ours': rlp_confs_ours,
                                                'sub_bboxes_ours': sub_bboxes_ours,
                                                'obj_bboxes_ours': obj_bboxes_ours})


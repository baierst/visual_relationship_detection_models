from keras.applications import VGG19
from keras.layers import Dense
from keras.models import Model, Sequential
from keras.callbacks import EarlyStopping
from keras.preprocessing.image import ImageDataGenerator
import pickle as pkl
import numpy as np
import h5py
from keras.utils import np_utils


def build_entity_model(weights='data/entity_model.w'):

    if weights is not None:
        image_model = VGG19()
    else:
        image_model = VGG19(weights='imagenet')

    rep_in = image_model.get_layer(index=0).input
    rep_out = image_model.get_layer(name='fc2').output

    rep_model = Model(rep_in, rep_out)

    model = Sequential()
    model.add(rep_model)
    model.add(Dense(100, activation='softmax', use_bias=False))

    if weights is not None:
        model.load_weights(weights)

    return model


def build_predicate_model(weights=None):

    image_model = VGG19(weights='imagenet')

    rep_in = image_model.get_layer(index=0).input
    rep_out = image_model.get_layer(name='fc2').output

    rep_model = Model(rep_in, rep_out)

    model = Sequential()
    model.add(rep_model)

    model.add(Dense(70, activation='softmax', use_bias=True))

    if weights is not None:
        model.load_weights(weights)

    return model


def train_entity_model(save_path=None):

    model = build_entity_model('data/entity_model.w')

    from keras.applications.imagenet_utils import preprocess_input

    datagen = ImageDataGenerator(
        preprocessing_function=preprocess_input,
        rotation_range=0.4,
        width_shift_range=0.4,
        height_shift_range=0.4,
        shear_range=0.4,
        zoom_range=0.4,
        channel_shift_range=0.4,
        horizontal_flip=True)

    index_to_label = pkl.load(open('data/index_to_object.pkl', 'rb'), encoding='latin1')
    classes = [index_to_label[i] for i in range(100)]

    train_generator = datagen.flow_from_directory(
        'data/objects_train',
        target_size=(224, 224),
        batch_size=32,
        classes=classes)


    for layer in model.layers[0].layers:
        layer.trainable = False

    model.compile(loss='categorical_crossentropy',
                  optimizer='rmsprop',
                  metrics=['categorical_accuracy'])

    early_stopping = EarlyStopping(monitor='val_categorical_accuracy', patience=30)

    model.fit_generator(train_generator, steps_per_epoch=800, epochs=500, verbose=1, validation_split=0.1, callbacks=[early_stopping])

    for layer in model.layers[0].layers[-9:]:
        layer.trainable = True

    from keras import optimizers

    model.compile(loss='categorical_crossentropy',
                           optimizer=optimizers.SGD(lr=1e-4, momentum=0.9),
                           metrics=['categorical_accuracy'])

    model.fit_generator(train_generator, steps_per_epoch=800, epochs=500, verbose=1, validation_split=0.1,
                        callbacks=[early_stopping])

    for layer in model.layers[0].layers:
        layer.trainable = True

    if save_path is not None:
        model.save_weights(save_path, overwrite=True)

    return model


def train_predicate_model(weights=None):
    model = build_predicate_model()

    from keras.applications.imagenet_utils import preprocess_input

    datagen = ImageDataGenerator(
        preprocessing_function=preprocess_input,
        rotation_range=0.4,
        width_shift_range=0.4,
        height_shift_range=0.4,
        shear_range=0.4,
        zoom_range=0.4,
        channel_shift_range=0.4,
        horizontal_flip=True)

    index_to_label = pkl.load(open('data/index_to_predicate.pkl', 'rb'), encoding='latin1')
    classes = [index_to_label[i] for i in range(70)]

    train_generator = datagen.flow_from_directory(
        'data/relations_train',
        target_size=(224, 224),
        batch_size=32,
        classes=classes)

    for layer in model.layers[0].layers:
        layer.trainable = False

    model.compile(loss='categorical_crossentropy',
                  optimizer='rmsprop',
                  metrics=['categorical_accuracy'])

    early_stopping = EarlyStopping(monitor='val_loss', patience=30)

    model.fit_generator(train_generator, steps_per_epoch=800, epochs=500, verbose=1, validation_split=0.1, callbacks=[early_stopping])

    if weights is not None:
        model.save_weights(weights)

    return model


def build_vgg_representation_model(weights=None):

    image_model = VGG19(weights='imagenet')

    rep_in = image_model.get_layer(index=0).input
    rep_out = image_model.get_layer(name='fc2').output

    model = Model(rep_in, rep_out)

    if weights is not None:
        model.load_weights(weights)

    return model


def test_entity_model():
    model = build_entity_model('data/entity_model.w')

    from keras.applications.imagenet_utils import preprocess_input    

    index_to_label = pkl.load(open('data/index_to_object.pkl', 'rb'), encoding='latin1')
    classes = [index_to_label[i] for i in range(100)]

    X_test = np.array(h5py.File('data/objects_X_test.h5', 'r')['data'])[:, ::-1, :, :] # BGR -> RGB
    y_test = np.array(h5py.File('data/objects_y_test.h5', 'r')['data'])
    Y_test = np_utils.to_categorical(y_test, 100)

    X_test = preprocess_input(X_test.astype('float32'))

    model.compile(loss='categorical_crossentropy',
              optimizer='rmsprop',
              metrics=['categorical_accuracy'])

    evaluation = model.evaluate(X_test, Y_test)

    return evaluation

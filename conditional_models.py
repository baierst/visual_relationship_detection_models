from keras.layers import Input, Multiply, Embedding, Flatten, Lambda, Concatenate, Dense, Merge, Dropout
from keras.optimizers import Adam, SGD
from keras.models import Model
from keras.callbacks import EarlyStopping
import keras.backend as K
from keras.utils import np_utils
import numpy as np
import pickle as pkl
from utils.image_utils import ImageLoader, extract_region, get_bbox_p
import scipy.io as sio
from utils.embedding3d import Embedding3D
from img_models import build_vgg_representation_model
import cv2

NB_ENTITIES = 100
NB_PREDICATES = 70


def build_multiway_model_no_image(rank):

    s_input = Input(shape=(1,), dtype='int32', name='subject_input')
    o_input = Input(shape=(1,), dtype='int32', name='object_input')

    entityEmbedding = Embedding(output_dim=rank, input_dim=NB_ENTITIES, input_length=1, name='embedding_entity')

    s_embedding = entityEmbedding(s_input)
    s_embedding = Flatten()(s_embedding)

    o_embedding = entityEmbedding(o_input)
    o_embedding = Flatten()(o_embedding)

    rep = Concatenate()([s_embedding, o_embedding])

    h = Dense(100, activation='relu')(rep)

    out = Dense(70, activation='softmax')(h)

    model = Model(inputs=[s_input, o_input], outputs=out)

    return model


def build_multiway_model_with_image(rank):

    s_input = Input(shape=(1,), dtype='int32', name='subject_input')
    o_input = Input(shape=(1,), dtype='int32', name='object_input')
    ip_rep = Input(shape=(4096,), dtype='int32', name='predicate_rep_input')

    entityEmbedding = Embedding(output_dim=rank, input_dim=NB_ENTITIES, input_length=1, name='embedding_entity')

    s_embedding = entityEmbedding(s_input)
    s_embedding = Flatten()(s_embedding)

    ip_rep_id = Dense(4096, kernel_initializer="identity", trainable=False)(ip_rep)

    ip_rep_dropout = Dropout(0.8)(ip_rep_id)
    ip_rep_small = Dense(rank)(ip_rep_dropout)

    o_embedding = entityEmbedding(o_input)
    o_embedding = Flatten()(o_embedding)

    rep = Concatenate()([s_embedding, o_embedding, ip_rep_small])

    h = Dense(100, activation='relu')(rep)

    out = Dense(70, activation='softmax')(h)

    model = Model(inputs=[s_input, o_input, ip_rep], outputs=out)
    return model


def load_train_data_with_image():

    annotations_train = pkl.load(open('data/annotations_train.pkl', 'rb'), encoding='latin1')
    train_loader = ImageLoader('data/train', buffer_size=1)

    vgg_rep_model = build_vgg_representation_model()

    x_train = []
    y_train = []

    img_count = 0

    for image in train_loader:

        if img_count % 100 == 0:
            print(img_count)
        img_count += 1

        def get_region(box, bgr):
            region = extract_region(bgr, box, rcnn=False)
            region = cv2.resize(region.astype('float32'), (224, 224)).transpose(2, 0, 1)
            return region

        if annotations_train[image[0]['filename']]['tuples'] == []:
            continue

        tuples = annotations_train[image[0]['filename']]['tuples']
        s, bs, p, bo, o = zip(*tuples)

        ###### P-REPRESENTATION #######
        bgr = cv2.imread('data/train/' + image[0]['filename'])
        regions = []
        for i in range(len(bs)):

            bbox_p = get_bbox_p(bs[i], bo[i])
            region = get_region(bbox_p, bgr)
            regions.append(region)

        p_reps = vgg_rep_model.predict(np.array(regions))

        x_train += zip(s, o, p_reps)
        y_train += p

    Y_train = np_utils.to_categorical(y_train, 70)


    x_train = list(zip(*x_train))
    X_train = [np.array(e) for e in x_train]

    return X_train, Y_train


def load_train_data_no_image():

    annotations_train = pkl.load(open('data/annotations_train.pkl', 'rb'), encoding='latin1')
    train_loader = ImageLoader('data/train', buffer_size=1)

    x_train = []
    y_train = []

    img_count = 0

    for image in train_loader:

        if img_count % 100 == 0:
            print(img_count)
        img_count += 1

        if annotations_train[image[0]['filename']]['tuples'] == []:
            continue

        tuples = annotations_train[image[0]['filename']]['tuples']
        s, bs, p, bo, o = zip(*tuples)
        x_train += zip(s, o)
        y_train += p

    Y_train = np_utils.to_categorical(y_train, 70)


    x_train = list(zip(*x_train))
    X_train = [np.array(e) for e in x_train]

    return X_train, Y_train

def save_test_ip_reps():

    filenames = pkl.load(open('data/filenames.pkl', 'rb'), encoding='latin1')
    annotations_test = pkl.load(open('data/annotations_test.pkl', 'rb'), encoding='latin1')
    annotations_rcnn = pkl.load(open('data/annotations_rcnn.pkl', 'rb'), encoding='latin1')


    def get_region(box, bgr):
        region = extract_region(bgr, box, rcnn=True)
        region = cv2.resize(region.astype('float32'), (224, 224)).transpose(2, 0, 1)
        return region

    vgg_rep_model = build_vgg_representation_model()

    test_ip_reps = []
    for i in range(0, 1000):

        if i % 100 == 0:
            print(i)

        filename = filenames[i][0]

        if annotations_test[filename]['tuples'] == [] or len(annotations_rcnn[filename]) < 2:
            test_ip_reps.append([])
            continue

        candidate_boxes = annotations_rcnn[filename]

        bgr = cv2.imread('data/test/' + filename)
        regions = [get_region(get_bbox_p(candidate_boxes[i_s], candidate_boxes[i_o]), bgr) for i_s in
                   range(len(candidate_boxes)) for i_o in range(len(candidate_boxes)) if i_s != i_o]
        ip_reps = vgg_rep_model.predict(np.array(regions))

        test_ip_reps.append(ip_reps)

    pkl.dump(test_ip_reps, open('data/test_ip_reps.pkl', 'wb'))


def train_conditional_with_image():

    X_train, Y_train = load_train_data_with_image()

    model = build_multiway_model_with_image(20)

    opt = SGD(lr=0.1)

    model.compile(opt, 'categorical_crossentropy', metrics=['categorical_accuracy'])

    model.fit(X_train, Y_train, batch_size=1024, epochs=500, verbose=2, validation_split=0.05)

    model.save_weights('data/conditional_with_image.w')

def train_conditional_no_image():

    X_train, Y_train = load_train_data_no_image()

    model = build_multiway_model_no_image(20)

    opt = Adam(lr=0.1)

    model.compile(opt, 'categorical_crossentropy', metrics=['categorical_accuracy'])

    model.fit(X_train, Y_train, batch_size=1024, epochs=500, verbose=2)

    model.save_weights('data/conditional_no_image.w')
